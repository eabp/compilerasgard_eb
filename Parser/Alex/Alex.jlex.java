/*
	1. Fase del Compilador para AsGard:
	Desarrollo de Alex - Analizador Léxico
	Equipo 4: 
	Ezequiel Bermúdez
	Diovilgy Salazar
*/
// Codigo de usuario
import java.lang.System;
class Alex{
	public static int errors = 0;
	public static int identif = 0;
	public static int reserved_words = 0;
	public static int reserved_words_structures = 0;
	public static int type_data = 0;
	public static int operator_predence = 0;
	public static int operator_a = 0;
	public static int operator_r = 0;
	public static int operator_c = 0;
    public static int operator_as = 0;
	public static int ctte = 0;
	public static int ignored = 0;
    public static void main (String argv[]) throws java.io.IOException {
        Yylex yy = new Yylex(System.in);
        System.out.println("Hola soy Alex, un Analizador Lexico generado por JLex");
    	System.out.println("Comienzo del Analisis\n (Fil, Col):\n");
        while (yy.yylex() != null);
    }
}
class Yytoken{
    Yytoken(){}
}   
//Directivas a JLEX


class Yylex {
	private final int YY_BUFFER_SIZE = 512;
	private final int YY_F = -1;
	private final int YY_NO_STATE = -1;
	private final int YY_NOT_ACCEPT = 0;
	private final int YY_START = 1;
	private final int YY_END = 2;
	private final int YY_NO_ANCHOR = 4;
	private final int YY_BOL = 65536;
	private final int YY_EOF = 65537;
	private java.io.BufferedReader yy_reader;
	private int yy_buffer_index;
	private int yy_buffer_read;
	private int yy_buffer_start;
	private int yy_buffer_end;
	private char yy_buffer[];
	private int yychar;
	private int yyline;
	private boolean yy_at_bol;
	private int yy_lexical_state;

	Yylex (java.io.Reader reader) {
		this ();
		if (null == reader) {
			throw (new Error("Error: Bad input stream initializer."));
		}
		yy_reader = new java.io.BufferedReader(reader);
	}

	Yylex (java.io.InputStream instream) {
		this ();
		if (null == instream) {
			throw (new Error("Error: Bad input stream initializer."));
		}
		yy_reader = new java.io.BufferedReader(new java.io.InputStreamReader(instream));
	}

	private Yylex () {
		yy_buffer = new char[YY_BUFFER_SIZE];
		yy_buffer_read = 0;
		yy_buffer_index = 0;
		yy_buffer_start = 0;
		yy_buffer_end = 0;
		yychar = 0;
		yyline = 0;
		yy_at_bol = true;
		yy_lexical_state = YYINITIAL;
	}

	private boolean yy_eof_done = false;
	private final int YYINITIAL = 0;
	private final int COMMENTS = 1;
	private final int yy_state_dtrans[] = {
		0,
		39
	};
	private void yybegin (int state) {
		yy_lexical_state = state;
	}
	private int yy_advance ()
		throws java.io.IOException {
		int next_read;
		int i;
		int j;

		if (yy_buffer_index < yy_buffer_read) {
			return yy_buffer[yy_buffer_index++];
		}

		if (0 != yy_buffer_start) {
			i = yy_buffer_start;
			j = 0;
			while (i < yy_buffer_read) {
				yy_buffer[j] = yy_buffer[i];
				++i;
				++j;
			}
			yy_buffer_end = yy_buffer_end - yy_buffer_start;
			yy_buffer_start = 0;
			yy_buffer_read = j;
			yy_buffer_index = j;
			next_read = yy_reader.read(yy_buffer,
					yy_buffer_read,
					yy_buffer.length - yy_buffer_read);
			if (-1 == next_read) {
				return YY_EOF;
			}
			yy_buffer_read = yy_buffer_read + next_read;
		}

		while (yy_buffer_index >= yy_buffer_read) {
			if (yy_buffer_index >= yy_buffer.length) {
				yy_buffer = yy_double(yy_buffer);
			}
			next_read = yy_reader.read(yy_buffer,
					yy_buffer_read,
					yy_buffer.length - yy_buffer_read);
			if (-1 == next_read) {
				return YY_EOF;
			}
			yy_buffer_read = yy_buffer_read + next_read;
		}
		return yy_buffer[yy_buffer_index++];
	}
	private void yy_move_end () {
		if (yy_buffer_end > yy_buffer_start &&
		    '\n' == yy_buffer[yy_buffer_end-1])
			yy_buffer_end--;
		if (yy_buffer_end > yy_buffer_start &&
		    '\r' == yy_buffer[yy_buffer_end-1])
			yy_buffer_end--;
	}
	private boolean yy_last_was_cr=false;
	private void yy_mark_start () {
		int i;
		for (i = yy_buffer_start; i < yy_buffer_index; ++i) {
			if ('\n' == yy_buffer[i] && !yy_last_was_cr) {
				++yyline;
			}
			if ('\r' == yy_buffer[i]) {
				++yyline;
				yy_last_was_cr=true;
			} else yy_last_was_cr=false;
		}
		yychar = yychar
			+ yy_buffer_index - yy_buffer_start;
		yy_buffer_start = yy_buffer_index;
	}
	private void yy_mark_end () {
		yy_buffer_end = yy_buffer_index;
	}
	private void yy_to_mark () {
		yy_buffer_index = yy_buffer_end;
		yy_at_bol = (yy_buffer_end > yy_buffer_start) &&
		            ('\r' == yy_buffer[yy_buffer_end-1] ||
		             '\n' == yy_buffer[yy_buffer_end-1] ||
		             2028/*LS*/ == yy_buffer[yy_buffer_end-1] ||
		             2029/*PS*/ == yy_buffer[yy_buffer_end-1]);
	}
	private java.lang.String yytext () {
		return (new java.lang.String(yy_buffer,
			yy_buffer_start,
			yy_buffer_end - yy_buffer_start));
	}
	private int yylength () {
		return yy_buffer_end - yy_buffer_start;
	}
	private char[] yy_double (char buf[]) {
		int i;
		char newbuf[];
		newbuf = new char[2*buf.length];
		for (i = 0; i < buf.length; ++i) {
			newbuf[i] = buf[i];
		}
		return newbuf;
	}
	private final int YY_E_INTERNAL = 0;
	private final int YY_E_MATCH = 1;
	private java.lang.String yy_error_string[] = {
		"Error: Internal error.\n",
		"Error: Unmatched input.\n"
	};
	private void yy_error (int code,boolean fatal) {
		java.lang.System.out.print(yy_error_string[code]);
		java.lang.System.out.flush();
		if (fatal) {
			throw new Error("Fatal Error.\n");
		}
	}
	private int[][] unpackFromString(int size1, int size2, String st) {
		int colonIndex = -1;
		String lengthString;
		int sequenceLength = 0;
		int sequenceInteger = 0;

		int commaIndex;
		String workString;

		int res[][] = new int[size1][size2];
		for (int i= 0; i < size1; i++) {
			for (int j= 0; j < size2; j++) {
				if (sequenceLength != 0) {
					res[i][j] = sequenceInteger;
					sequenceLength--;
					continue;
				}
				commaIndex = st.indexOf(',');
				workString = (commaIndex==-1) ? st :
					st.substring(0, commaIndex);
				st = st.substring(commaIndex+1);
				colonIndex = workString.indexOf(':');
				if (colonIndex == -1) {
					res[i][j]=Integer.parseInt(workString);
					continue;
				}
				lengthString =
					workString.substring(colonIndex+1);
				sequenceLength=Integer.parseInt(lengthString);
				workString=workString.substring(0,colonIndex);
				sequenceInteger=Integer.parseInt(workString);
				res[i][j] = sequenceInteger;
				sequenceLength--;
			}
		}
		return res;
	}
	private int yy_acpt[] = {
		/* 0 */ YY_NOT_ACCEPT,
		/* 1 */ YY_NO_ANCHOR,
		/* 2 */ YY_NO_ANCHOR,
		/* 3 */ YY_NO_ANCHOR,
		/* 4 */ YY_NO_ANCHOR,
		/* 5 */ YY_NO_ANCHOR,
		/* 6 */ YY_NO_ANCHOR,
		/* 7 */ YY_NO_ANCHOR,
		/* 8 */ YY_NO_ANCHOR,
		/* 9 */ YY_NO_ANCHOR,
		/* 10 */ YY_NO_ANCHOR,
		/* 11 */ YY_NO_ANCHOR,
		/* 12 */ YY_NO_ANCHOR,
		/* 13 */ YY_NO_ANCHOR,
		/* 14 */ YY_NO_ANCHOR,
		/* 15 */ YY_NO_ANCHOR,
		/* 16 */ YY_NO_ANCHOR,
		/* 17 */ YY_NO_ANCHOR,
		/* 18 */ YY_NO_ANCHOR,
		/* 19 */ YY_NO_ANCHOR,
		/* 20 */ YY_NOT_ACCEPT,
		/* 21 */ YY_NO_ANCHOR,
		/* 22 */ YY_NO_ANCHOR,
		/* 23 */ YY_NO_ANCHOR,
		/* 24 */ YY_NO_ANCHOR,
		/* 25 */ YY_NO_ANCHOR,
		/* 26 */ YY_NO_ANCHOR,
		/* 27 */ YY_NOT_ACCEPT,
		/* 28 */ YY_NO_ANCHOR,
		/* 29 */ YY_NO_ANCHOR,
		/* 30 */ YY_NO_ANCHOR,
		/* 31 */ YY_NO_ANCHOR,
		/* 32 */ YY_NOT_ACCEPT,
		/* 33 */ YY_NO_ANCHOR,
		/* 34 */ YY_NO_ANCHOR,
		/* 35 */ YY_NOT_ACCEPT,
		/* 36 */ YY_NO_ANCHOR,
		/* 37 */ YY_NOT_ACCEPT,
		/* 38 */ YY_NO_ANCHOR,
		/* 39 */ YY_NOT_ACCEPT,
		/* 40 */ YY_NO_ANCHOR,
		/* 41 */ YY_NOT_ACCEPT,
		/* 42 */ YY_NO_ANCHOR,
		/* 43 */ YY_NO_ANCHOR,
		/* 44 */ YY_NO_ANCHOR,
		/* 45 */ YY_NO_ANCHOR,
		/* 46 */ YY_NO_ANCHOR,
		/* 47 */ YY_NO_ANCHOR,
		/* 48 */ YY_NO_ANCHOR,
		/* 49 */ YY_NO_ANCHOR,
		/* 50 */ YY_NO_ANCHOR,
		/* 51 */ YY_NO_ANCHOR,
		/* 52 */ YY_NO_ANCHOR,
		/* 53 */ YY_NO_ANCHOR,
		/* 54 */ YY_NO_ANCHOR,
		/* 55 */ YY_NO_ANCHOR,
		/* 56 */ YY_NO_ANCHOR,
		/* 57 */ YY_NO_ANCHOR,
		/* 58 */ YY_NO_ANCHOR,
		/* 59 */ YY_NO_ANCHOR,
		/* 60 */ YY_NO_ANCHOR,
		/* 61 */ YY_NO_ANCHOR,
		/* 62 */ YY_NO_ANCHOR,
		/* 63 */ YY_NO_ANCHOR,
		/* 64 */ YY_NO_ANCHOR,
		/* 65 */ YY_NO_ANCHOR,
		/* 66 */ YY_NO_ANCHOR,
		/* 67 */ YY_NO_ANCHOR,
		/* 68 */ YY_NO_ANCHOR,
		/* 69 */ YY_NO_ANCHOR,
		/* 70 */ YY_NO_ANCHOR,
		/* 71 */ YY_NO_ANCHOR,
		/* 72 */ YY_NO_ANCHOR,
		/* 73 */ YY_NO_ANCHOR,
		/* 74 */ YY_NO_ANCHOR,
		/* 75 */ YY_NO_ANCHOR,
		/* 76 */ YY_NO_ANCHOR,
		/* 77 */ YY_NO_ANCHOR,
		/* 78 */ YY_NO_ANCHOR,
		/* 79 */ YY_NO_ANCHOR,
		/* 80 */ YY_NO_ANCHOR,
		/* 81 */ YY_NO_ANCHOR,
		/* 82 */ YY_NO_ANCHOR,
		/* 83 */ YY_NO_ANCHOR,
		/* 84 */ YY_NO_ANCHOR,
		/* 85 */ YY_NO_ANCHOR,
		/* 86 */ YY_NO_ANCHOR,
		/* 87 */ YY_NO_ANCHOR,
		/* 88 */ YY_NO_ANCHOR,
		/* 89 */ YY_NO_ANCHOR,
		/* 90 */ YY_NO_ANCHOR
	};
	private int yy_cmap[] = unpackFromString(1,65538,
"42:9,40,39,42,40,39,42:18,36,42:3,30,23,42:2,21:2,23:2,32,22,42,24,37:10,31" +
",32,27,25,26,42:2,38:26,44,29,44:2,35,30,10,6,19,8,7,13,5,14,3,38:2,15,18,4" +
",16,11,38,9,2,12,1,20,17,38,33,38,41,34,43,28,42:65409,0:2")[0];

	private int yy_rmap[] = unpackFromString(1,91,
"0,1,2,1:2,3,4,1:2,5,6,7,8,1:2,8,1,8,1:2,9,10,11,12,1,13,8,14,15,16,17,5,18," +
"19,1,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,4" +
"3,44,45,46,47,48,49,50,51,52,53,54,55,56,57,8,58,59,60,61,62,63,64,65,66,67" +
",68,69,70,8,71,72,73")[0];

	private int yy_nxt[][] = unpackFromString(74,45,
"1,2,87,21,87:2,88,51,68,69,87,89,28,70,87:3,71,87,90,87,3,4:2,22,5,23,29,34" +
",6,7,25,8,87,31,24,9,10,87,11,9,30,24:3,-1:46,87,72,87:18,-1:12,87,-1:3,73," +
"87,-1:32,13,-1:42,34,-1:54,9,-1,9,-1:2,9:2,-1:41,10,-1:41,9,-1,9,-1:2,11,9," +
"-1:5,87:20,-1:12,87,-1:3,73,87,-1:24,32,-1:27,87:3,74,87:8,12,87:7,-1:12,87" +
",-1:3,73,87,-1:31,34,-1:3,34,-1:40,34,-1,13,-1:42,8,-1:45,16,-1:19,87:8,55," +
"87:4,56,87,12,87:4,-1:12,87,-1:3,73,87,-1:13,20,-1:14,27,-1,27,34,13,-1:2,2" +
"7,-1:4,27:3,-1:30,14,-1:33,35,-1:34,87:7,15,87:12,-1:12,87,-1:3,73,87,-1:18" +
",37,-1:33,87:6,12,87:13,-1:12,87,-1:3,73,87,-1:39,27,-1:12,87:6,26,87:13,-1" +
":12,87,-1:3,73,87,-1:6,1,18:21,41,18:18,-1,18,-1:3,87:3,12,87:16,-1:12,87,-" +
"1:3,73,87,-1:49,19,-1:2,87:17,12,87:2,-1:12,87,-1:3,73,87,-1:7,87:13,12,87:" +
"6,-1:12,87,-1:3,73,87,-1:7,87:4,15,87:15,-1:12,87,-1:3,73,87,-1:7,87:3,15,8" +
"7:16,-1:12,87,-1:3,73,87,-1:7,87:11,15,87:8,-1:12,87,-1:3,73,87,-1:7,87:11," +
"12,87:8,-1:12,87,-1:3,73,87,-1:7,87,17,87:18,-1:12,87,-1:3,73,87,-1:7,87:8," +
"17,87:11,-1:12,87,-1:3,73,87,-1:7,87:3,17,87:16,-1:12,87,-1:3,73,87,-1:7,87" +
":3,33,87:10,52,87:5,-1:12,87,-1:3,73,87,-1:7,87,36,87:18,-1:12,87,-1:3,73,8" +
"7,-1:7,87:3,36,87:16,-1:12,87,-1:3,73,87,-1:7,87:9,33,83,87:9,-1:12,87,-1:3" +
",73,87,-1:7,38,87:19,-1:12,87,-1:3,73,87,-1:7,87:6,40,87:13,-1:12,87,-1:3,7" +
"3,87,-1:7,87:15,42,87:4,-1:12,87,-1:3,73,87,-1:7,87:11,43,87:8,-1:12,87,-1:" +
"3,73,87,-1:7,87:3,44,87:16,-1:12,87,-1:3,73,87,-1:7,87:2,45,87:17,-1:12,87," +
"-1:3,73,87,-1:7,87:3,46,87:16,-1:12,87,-1:3,73,87,-1:7,87,38,87:18,-1:12,87" +
",-1:3,73,87,-1:7,87:14,36,87:5,-1:12,87,-1:3,73,87,-1:7,87:9,47,87:10,-1:12" +
",87,-1:3,73,87,-1:7,87:9,48,87:10,-1:12,87,-1:3,73,87,-1:7,87:6,49,87:13,-1" +
":12,87,-1:3,73,87,-1:7,87:9,50,87:10,-1:12,87,-1:3,73,87,-1:7,87:15,53,87:4" +
",-1:12,87,-1:3,73,87,-1:7,87:6,54,87:13,-1:12,87,-1:3,73,87,-1:7,87:8,57,78" +
",87:10,-1:12,87,-1:3,73,87,-1:7,87:2,58,87:10,79,87:6,-1:12,87,-1:3,73,87,-" +
"1:7,87:2,59,87:17,-1:12,87,-1:3,73,87,-1:7,87:11,81,87:8,-1:12,87,-1:3,73,8" +
"7,-1:7,87:4,60,87:15,-1:12,87,-1:3,73,87,-1:7,87:15,82,87:4,-1:12,87,-1:3,7" +
"3,87,-1:7,87:2,61,87:17,-1:12,87,-1:3,73,87,-1:7,87:14,62,87:5,-1:12,87,-1:" +
"3,73,87,-1:7,87:2,63,87:17,-1:12,87,-1:3,73,87,-1:7,87:3,84,87:16,-1:12,87," +
"-1:3,73,87,-1:7,87:6,85,87:13,-1:12,87,-1:3,73,87,-1:7,87:14,86,87:5,-1:12," +
"87,-1:3,73,87,-1:7,87:6,64,87:13,-1:12,87,-1:3,73,87,-1:7,87:19,65,-1:12,87" +
",-1:3,73,87,-1:7,87:4,66,87:15,-1:12,87,-1:3,73,87,-1:7,87:6,67,87:13,-1:12" +
",87,-1:3,73,87,-1:7,87:6,75,87:8,76,87:4,-1:12,87,-1:3,73,87,-1:7,87:8,77,8" +
"7:11,-1:12,87,-1:3,73,87,-1:7,87:9,80,87:10,-1:12,87,-1:3,73,87,-1:6");

	public Yytoken yylex ()
		throws java.io.IOException {
		int yy_lookahead;
		int yy_anchor = YY_NO_ANCHOR;
		int yy_state = yy_state_dtrans[yy_lexical_state];
		int yy_next_state = YY_NO_STATE;
		int yy_last_accept_state = YY_NO_STATE;
		boolean yy_initial = true;
		int yy_this_accept;

		yy_mark_start();
		yy_this_accept = yy_acpt[yy_state];
		if (YY_NOT_ACCEPT != yy_this_accept) {
			yy_last_accept_state = yy_state;
			yy_mark_end();
		}
		while (true) {
			if (yy_initial && yy_at_bol) yy_lookahead = YY_BOL;
			else yy_lookahead = yy_advance();
			yy_next_state = YY_F;
			yy_next_state = yy_nxt[yy_rmap[yy_state]][yy_cmap[yy_lookahead]];
			if (YY_EOF == yy_lookahead && true == yy_initial) {

    {
    	System.out.println("\nAnalisis Finalizado");
    	System.out.println("Resultados:\n");
    	System.out.println("Cantidad de Identificadores analizados: " + Alex.identif);
    	System.out.println("Errores de Encontrados: " + Alex.errors);
    	System.out.println("Palabras Reservadas Basicas: " + Alex.reserved_words);
    	System.out.println("Palabras Reservadas de Estructuras: " + Alex.reserved_words_structures);
    	System.out.println("Tipos de Datos: " + Alex.type_data);
    	System.out.println("Operador de Precedencia: " + Alex.operator_predence);
    	System.out.println("Operador Aritmetico: " + Alex.operator_a);
    	System.out.println("Operador Relacional: " + Alex.operator_r);
    	System.out.println("Operador de Canvas: " + Alex.operator_c);
        System.out.println("Operador de Asignacion o Secuenciacion: " + Alex.operator_as);
    	System.out.println("Constantes: " + Alex.ctte);
    	System.out.println("Cantidad de identf ignoradas: " + Alex.ignored);
    	System.exit(0);
    }
			}
			if (YY_F != yy_next_state) {
				yy_state = yy_next_state;
				yy_initial = false;
				yy_this_accept = yy_acpt[yy_state];
				if (YY_NOT_ACCEPT != yy_this_accept) {
					yy_last_accept_state = yy_state;
					yy_mark_end();
				}
			}
			else {
				if (YY_NO_STATE == yy_last_accept_state) {
					throw (new Error("Lexical Error: Unmatched Input."));
				}
				else {
					yy_anchor = yy_acpt[yy_last_accept_state];
					if (0 != (YY_END & yy_anchor)) {
						yy_move_end();
					}
					yy_to_mark();
					switch (yy_last_accept_state) {
					case 1:
						
					case -2:
						break;
					case 2:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -3:
						break;
					case 3:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Operator Precedence: " + yytext());
    Alex.operator_predence++;
}
					case -4:
						break;
					case 4:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Operator Arithmetic: " + yytext());
    Alex.operator_a++;
}
					case -5:
						break;
					case 5:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Operator Relational: " + yytext());
    Alex.operator_r++;
}
					case -6:
						break;
					case 6:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Error lexico no se renoce la siguiente instruccion: " + yytext());
    Alex.errors++;
    Alex.identif++;
    yychar = 0;
}
					case -7:
						break;
					case 7:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Operator Canvas: " + yytext());
    Alex.operator_c++;
}
					case -8:
						break;
					case 8:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Asignacion o Secuenciacion: " + yytext());
    Alex.operator_as++;
}
					case -9:
						break;
					case 9:
						{}
					case -10:
						break;
					case 10:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Number ctte:" + yytext());
    Alex.identif++;
}
					case -11:
						break;
					case 11:
						{
	yychar = 0;
}
					case -12:
						break;
					case 12:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Reserved Word Structure: " + yytext());
    Alex.reserved_words_structures++;
}
					case -13:
						break;
					case 13:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Error lexico no se renoce la siguiente instruccion: " + yytext());
    Alex.errors++;
    Alex.identif++;
    yychar = 0;
}
					case -14:
						break;
					case 14:
						{
    yybegin(COMMENTS);
}
					case -15:
						break;
					case 15:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Reserved Words: " + yytext());
    Alex.reserved_words++;
}
					case -16:
						break;
					case 16:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Constants: " + yytext());
    Alex.ctte++;
}
					case -17:
						break;
					case 17:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Type Data: " + yytext());
    Alex.type_data++;
}
					case -18:
						break;
					case 18:
						{
	Alex.ignored++;
}
					case -19:
						break;
					case 19:
						{
    yybegin(YYINITIAL);
}
					case -20:
						break;
					case 21:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -21:
						break;
					case 22:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Operator Arithmetic: " + yytext());
    Alex.operator_a++;
}
					case -22:
						break;
					case 23:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Operator Relational: " + yytext());
    Alex.operator_r++;
}
					case -23:
						break;
					case 24:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Error lexico no se renoce la siguiente instruccion: " + yytext());
    Alex.errors++;
    Alex.identif++;
    yychar = 0;
}
					case -24:
						break;
					case 25:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Operator Canvas: " + yytext());
    Alex.operator_c++;
}
					case -25:
						break;
					case 26:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Constants: " + yytext());
    Alex.ctte++;
}
					case -26:
						break;
					case 28:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -27:
						break;
					case 29:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Operator Relational: " + yytext());
    Alex.operator_r++;
}
					case -28:
						break;
					case 30:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Error lexico no se renoce la siguiente instruccion: " + yytext());
    Alex.errors++;
    Alex.identif++;
    yychar = 0;
}
					case -29:
						break;
					case 31:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Operator Canvas: " + yytext());
    Alex.operator_c++;
}
					case -30:
						break;
					case 33:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -31:
						break;
					case 34:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Operator Relational: " + yytext());
    Alex.operator_r++;
}
					case -32:
						break;
					case 36:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -33:
						break;
					case 38:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -34:
						break;
					case 40:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -35:
						break;
					case 42:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -36:
						break;
					case 43:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -37:
						break;
					case 44:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -38:
						break;
					case 45:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -39:
						break;
					case 46:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -40:
						break;
					case 47:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -41:
						break;
					case 48:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -42:
						break;
					case 49:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -43:
						break;
					case 50:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -44:
						break;
					case 51:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -45:
						break;
					case 52:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -46:
						break;
					case 53:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -47:
						break;
					case 54:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -48:
						break;
					case 55:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -49:
						break;
					case 56:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -50:
						break;
					case 57:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -51:
						break;
					case 58:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -52:
						break;
					case 59:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -53:
						break;
					case 60:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -54:
						break;
					case 61:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -55:
						break;
					case 62:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -56:
						break;
					case 63:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -57:
						break;
					case 64:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -58:
						break;
					case 65:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -59:
						break;
					case 66:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -60:
						break;
					case 67:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -61:
						break;
					case 68:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -62:
						break;
					case 69:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -63:
						break;
					case 70:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -64:
						break;
					case 71:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -65:
						break;
					case 72:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -66:
						break;
					case 73:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -67:
						break;
					case 74:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -68:
						break;
					case 75:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -69:
						break;
					case 76:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -70:
						break;
					case 77:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -71:
						break;
					case 78:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -72:
						break;
					case 79:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -73:
						break;
					case 80:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -74:
						break;
					case 81:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -75:
						break;
					case 82:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -76:
						break;
					case 83:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -77:
						break;
					case 84:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -78:
						break;
					case 85:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -79:
						break;
					case 86:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -80:
						break;
					case 87:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -81:
						break;
					case 88:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -82:
						break;
					case 89:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -83:
						break;
					case 90:
						{
    System.out.print("\n(" + yyline + "," + yychar + "): ");
    System.out.println("Identificador: " + yytext());
    Alex.identif++;
}
					case -84:
						break;
					default:
						yy_error(YY_E_INTERNAL,false);
					case -1:
					}
					yy_initial = true;
					yy_state = yy_state_dtrans[yy_lexical_state];
					yy_next_state = YY_NO_STATE;
					yy_last_accept_state = YY_NO_STATE;
					yy_mark_start();
					yy_this_accept = yy_acpt[yy_state];
					if (YY_NOT_ACCEPT != yy_this_accept) {
						yy_last_accept_state = yy_state;
						yy_mark_end();
					}
				}
			}
		}
	}
}
