Reglas:
Un programa consta primero de una secci�n de declaraciones (puede ser nula) donde se definen las variables que luego se utilizan en el cuerpo, y luego una secci�n de c�digo (obligatoria).

	Programa ::= Declaraciones Cuerpo | Cuerpo;

Como en la secci�n de Declaraciones (cuando no es nula) puede haber una lista de declaraciones, entonces definimos la regla para una declaraci�n y luego la regla para la lista de declaraciones.

	Declaracion ::= INT ID PTOCOMA;
	Declaraciones ::= Declaracion Declaraciones | Declaracion;

El cuerpo de un programa debe comenzar con main(), seguido de una llave "{", seguido de un bloque de sentencias y finalizando con una llave "}".

	Cuerpo ::= MAIN LPAREN RPAREN LLLAVE BloqueSentencias RLLAVE;

El bloque de sentencias puede tener ninguna, una, o varias sentencias.
	
	BloqueSentencias ::= Sentencias | ;
	Sentencias ::= Sentencias Sentencia | Sentencia;

En el cuerpo del programa solo pueden aparecer las sentencias: Condicional, Repetici�n (while), Asignaci�n, Salida est�ndar y Break.

	Sentencia ::= SentIf | SentWhile | SentAsignacion | SentPutw | SentPuts | SentBreak;

	SentIf ::= IF LPAREN Condicion RPAREN LLLAVE BloqueSentencias RLLAVE SentElse;
	SentElse ::= ELSE LLLAVE BloqueSentencias RLLAVE | ;
	Condicion ::= Expresion OR Expresion | Expresion AND Expresion | Expresion IGUAL Expresion |
		      Expresion DISTINTO Expresion | Expresion MAYOR Expresion | Expresion MENOR Expresion |
		      LPAREN Condicion RPAREN;

	SentWhile ::= WHILE LPAREN Condicion RPAREN LLLAVE BloqueSentencias RLLAVE;

	SentAsignacion ::= ID ASIGNAR Expresion PTOCOMA;

	SentPutw ::= PUTW LPAREN Expresion RPAREN PTOCOMA;
	SentPuts ::= PUTS LPAREN CADENATEXTO RPAREN PTOCOMA;

	SentBreak ::= BREAK PTOCOMA;

El tipo de expresiones posibles son:

	Expresion ::= Expresion SUMA Expresion | Expresion RESTA Expresion | Expresion PRODUCTO Expresion |
		      Expresion DIVISION Expresion | ENTERO | ID |
		      LPAREN Expresion RPAREN; 